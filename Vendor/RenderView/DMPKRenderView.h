//
//  DMPKRenderView.h
//  Peak
//
//  Created by Oleksandr Shakhmin on 6/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IMCustomNative;

typedef void (^DMPKRenderViewActionHandler)();
typedef void (^DMPKRenderViewPrivacyActionHandler)();
typedef void (^DMPKRenderViewDidShowHandler)();

@interface DMPKRenderView : UIView

@property(strong, nonatomic) DMPKRenderViewActionHandler actionHandler;
@property(strong, nonatomic) DMPKRenderViewPrivacyActionHandler privacyActionHandler;
@property(strong, nonatomic) DMPKRenderViewDidShowHandler didShowHandler;

- (void)renderAd:(IMCustomNative *)ad;

@end
