//
//  DMPKRenderView.m
//  Peak
//
//  Created by Oleksandr Shakhmin on 6/22/16.
//  Copyright © 2016 NIX. All rights reserved.
//

#import "DMPKRenderView.h"
#import "IMCustomNative.h"

@interface DMPKRenderView ()

@property(strong, nonatomic) UILabel *title;
@property(strong, nonatomic) UILabel *text;
@property(strong, nonatomic) UIImageView *icon;
@property(strong, nonatomic) UIImageView *mainImage;
@property(strong, nonatomic) UIButton *privacyAction;
@property(strong, nonatomic) UIButton *action;
@property(strong, nonatomic) UIButton *close;
@property(strong, nonatomic) UILabel *sponsoredText;

@end

@implementation DMPKRenderView

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        [self setupViews];
        [self setupLayout];
    }
    
    return self;
}

- (void)renderAd:(IMCustomNative *)ad
{
    NSError *jsonError;
    NSData *data = [[ad adContent] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    
    [_title setText:json[@"title"]];
    [_text setText:json[@"description"]];
    [_icon setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:json[@"icon"][@"url"]]]]];
    [_mainImage setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:json[@"screenshots"][@"url"]]]]];
    [_action setTitle:json[@"cta"] forState:UIControlStateNormal];
    
    [self setNeedsLayout];
    
    dispatch_async(dispatch_get_main_queue(), ^
    {
        if (_didShowHandler != nil)
        {
            _didShowHandler();
        }
    });
}

#pragma mark - Private

- (void)setupViews
{
    [self setBackgroundColor:[UIColor whiteColor]];
    
    _close = [UIButton buttonWithType:UIButtonTypeSystem];
    [_close setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_close setTitle:@"Close ╳" forState:UIControlStateNormal];
    [_close setTintColor:[UIColor redColor]];
    [_close addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_close];
    
    _title = [UILabel new];
    [_title setFont:[UIFont boldSystemFontOfSize:14.f]];
    [_title setNumberOfLines:0];
    [_title setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:_title];
    
    _sponsoredText = [UILabel new];
    [_sponsoredText setFont:[UIFont systemFontOfSize:12.f]];
    [_sponsoredText setNumberOfLines:0];
    [_sponsoredText setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_sponsoredText setText:@"Sponsored"];
    [_sponsoredText setTextColor:[UIColor darkGrayColor]];
    [self addSubview:_sponsoredText];
    
    _icon = [UIImageView new];
    [_icon setBackgroundColor:[UIColor grayColor]];
    [_icon setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:_icon];
    
    _text = [UILabel new];
    [_text setFont:[UIFont systemFontOfSize:14.f]];
    [_text setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_text setNumberOfLines:0];
    [self addSubview:_text];
    
    _mainImage = [UIImageView new];
    [_mainImage setBackgroundColor:[UIColor grayColor]];
    [_mainImage setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:_mainImage];
    
    _action = [UIButton buttonWithType:UIButtonTypeSystem];
    [_action setTintColor:[UIColor orangeColor]];
    [_action setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_action addTarget:self action:@selector(performAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_action];
    
    _privacyAction = [UIButton buttonWithType:UIButtonTypeSystem];
    [_privacyAction setTintColor:[UIColor orangeColor]];
    [_privacyAction setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_privacyAction addTarget:self action:@selector(performPrivacyAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_privacyAction];
}

- (void)setupLayout
{
    // Vertical
    static const CGFloat statusBarHeight = 20.f;
    static const CGFloat closeHeight = 16.f;
    static const CGFloat iconHeight = 30.f;
    static const CGFloat mainImageHeight = 120.f;
    static const CGFloat actionHeight = 30.f;
    static const CGFloat privacyActionHeight = 30.f;
    
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:|-statusBarHeight-[_close(==closeHeight)]-[_title(>=iconHeight)]-[_text]-[_mainImage(==mainImageHeight)]-[_action(==actionHeight)]"
                          options:NSLayoutFormatDirectionLeadingToTrailing
                          metrics:@{@"iconHeight" : @(iconHeight),
                                    @"statusBarHeight" : @(statusBarHeight),
                                    @"closeHeight" : @(closeHeight),
                                    @"mainImageHeight" : @(mainImageHeight),
                                    @"actionHeight" : @(actionHeight)}
                          views:NSDictionaryOfVariableBindings(_close, _title, _text, _mainImage, _action, _privacyAction)]];

    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:[_icon(==iconHeight)]"
                          options:NSLayoutFormatDirectionLeadingToTrailing
                          metrics:@{@"iconHeight" : @(iconHeight)}
                          views:NSDictionaryOfVariableBindings(_icon)]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_icon
                         attribute:NSLayoutAttributeTop
                         relatedBy:NSLayoutRelationEqual
                         toItem:_title
                         attribute:NSLayoutAttributeTop
                         multiplier:1.0
                         constant:0]];
    
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:[_sponsoredText(==iconHeight)]"
                          options:NSLayoutFormatDirectionLeadingToTrailing
                          metrics:@{@"iconHeight" : @(iconHeight)}
                          views:NSDictionaryOfVariableBindings(_sponsoredText)]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_sponsoredText
                         attribute:NSLayoutAttributeTop
                         relatedBy:NSLayoutRelationEqual
                         toItem:_title
                         attribute:NSLayoutAttributeTop
                         multiplier:1.0
                         constant:0]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_action
                         attribute:NSLayoutAttributeBottom
                         relatedBy:NSLayoutRelationLessThanOrEqual
                         toItem:self
                         attribute:NSLayoutAttributeBottom
                         multiplier:1.0
                         constant:0]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_action
                         attribute:NSLayoutAttributeHeight
                         relatedBy:NSLayoutRelationLessThanOrEqual
                         toItem:nil
                         attribute:NSLayoutAttributeNotAnAttribute
                         multiplier:1.0
                         constant:iconHeight]];
    
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"V:[_privacyAction(==privacyActionHeight)]"
                          options:NSLayoutFormatDirectionLeadingToTrailing
                          metrics:@{@"privacyActionHeight" : @(privacyActionHeight)}
                          views:NSDictionaryOfVariableBindings(_privacyAction)]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_privacyAction
                         attribute:NSLayoutAttributeTop
                         relatedBy:NSLayoutRelationEqual
                         toItem:_action
                         attribute:NSLayoutAttributeTop
                         multiplier:1.0
                         constant:0]];
    
    // Horizontal
    static const CGFloat iconWidth = 30.f;
    static const CGFloat iconAndTitleHorizontalOffset = 8.f;
    static const CGFloat privacyActionWidth = 30.f;
    static const CGFloat privacyActionAndActionHorizontalOffset = 8.f;
    static const CGFloat sponsoredTextWidth = 70.f;
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_close
                         attribute:NSLayoutAttributeTrailing
                         relatedBy:NSLayoutRelationEqual
                         toItem:self
                         attribute:NSLayoutAttributeTrailing
                         multiplier:1.0
                         constant:0]];
    
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"H:|-[_icon(==iconWidth)]-iconAndTitleHorizontalOffset-[_title]-[_sponsoredText(==sponsoredTextWidth)]-|"
                          options:NSLayoutFormatDirectionLeadingToTrailing
                          metrics:@{@"iconWidth" : @(iconWidth),
                                    @"iconAndTitleHorizontalOffset" : @(iconAndTitleHorizontalOffset),
                                    @"sponsoredTextWidth" : @(sponsoredTextWidth)}
                          views:NSDictionaryOfVariableBindings(_icon, _title, _sponsoredText)]];
    
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"H:|-[_text]-|"
                          options:NSLayoutFormatDirectionLeadingToTrailing
                          metrics:nil
                          views:NSDictionaryOfVariableBindings(_text)]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_mainImage
                         attribute:NSLayoutAttributeLeading
                         relatedBy:NSLayoutRelationGreaterThanOrEqual
                         toItem:self
                         attribute:NSLayoutAttributeLeading
                         multiplier:1.0
                         constant:0]];
    
    [self addConstraint:[NSLayoutConstraint
                         constraintWithItem:_mainImage
                         attribute:NSLayoutAttributeCenterX
                         relatedBy:NSLayoutRelationEqual
                         toItem:self
                         attribute:NSLayoutAttributeCenterX
                         multiplier:1.0
                         constant:0]];
    
    [self addConstraints:[NSLayoutConstraint
                          constraintsWithVisualFormat:@"H:|-[_privacyAction(==privacyActionWidth)]-privacyActionAndActionHorizontalOffset-[_action]-|"
                          options:NSLayoutFormatDirectionLeadingToTrailing
                          metrics:@{@"privacyActionWidth" : @(privacyActionWidth),
                                    @"privacyActionAndActionHorizontalOffset" : @(privacyActionAndActionHorizontalOffset)}
                          views:NSDictionaryOfVariableBindings(_privacyAction, _action)]];
}

- (void)close:(id)sender
{
    [self removeFromSuperview];
}

- (void)performAction:(id)sender
{
    if (_actionHandler != nil)
    {
        _actionHandler();
    }
}

- (void)performPrivacyAction:(id)sender
{
    if (_privacyActionHandler != nil)
    {
        _privacyActionHandler();
    }
}

@end
