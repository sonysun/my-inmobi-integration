//
//  AppDelegate.h
//  inMobyTestIntegration
//
//  Created by Alex Nadein on 10/18/16.
//  Copyright © 2016 Peak Mediation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (void)printClassWithName:(NSString *)className;

@end

