//
//  AppDelegate.m
//  inMobyTestIntegration
//
//  Created by Alex Nadein on 10/18/16.
//  Copyright © 2016 Peak Mediation. All rights reserved.
//

#import "AppDelegate.h"
#import "IMSdk.h"
#import <objc/runtime.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (void)printClassWithName:(NSString *)className
{
    NSLog(@"Class: %@", className);
    
    NSLog(@"----");
    
    unsigned int numIvars = 0;
    Ivar *ivars = class_copyIvarList(NSClassFromString(className), &numIvars);
    
    for (int i = 0; i < numIvars; ++i)
    {
        const char *name = ivar_getName(ivars[i]);
        NSLog(@"Ivar: %@", [NSString stringWithCString:name encoding:NSUTF8StringEncoding]);
    }
    
    free(ivars);
    
    NSLog(@"----");
    
    unsigned int numMethods = 0;
    Method *methods = class_copyMethodList(NSClassFromString(className), &numMethods);
    
    for (int i = 0; i < numMethods; ++i)
    {
        SEL selector = method_getName(methods[i]);
        NSLog(@"Method: %@", NSStringFromSelector(selector));
    }
    
    free(methods);
    
    NSLog(@"----------------");
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [IMSdk initWithAccountID:@"27048aa4a5694fc68e993f042b8d4650"];
    [IMSdk setLogLevel:kIMSDKLogLevelDebug];
    return YES;
}

@end
