//
//  ViewController.m
//  inMobyTestIntegration
//
//  Created by Alex Nadein on 10/18/16.
//  Copyright © 2016 Peak Mediation. All rights reserved.
//

#import "ViewController.h"
#import "IMSdk.h"
#import "IMBanner.h"
#import "IMInterstitial.h"
#import "IMCustomNative.h"
#import "DMPKRenderView.h"
#import "AppDelegate.h"

#define INMOBI_BANNER_FRAME CGRectMake(0, 0, 320, 50)

@interface ViewController () <IMBannerDelegate, IMInterstitialDelegate, IMNativeDelegate>

#pragma mark - Properties

@property(nonatomic, strong) IMBanner *banner;
@property(nonatomic, strong) IMInterstitial *interstitialStatic;
@property(nonatomic, strong) IMInterstitial *interstitialVideo;
@property(nonatomic, strong) IMInterstitial *interstitialRewarded;
@property(nonatomic, strong) IMCustomNative *native;
@property(nonatomic, strong) DMPKRenderView *renderView;

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UIView *bannerIndicator;
@property (weak, nonatomic) IBOutlet UIView *staticIndicator;
@property (weak, nonatomic) IBOutlet UIView *videoIndicator;
@property (weak, nonatomic) IBOutlet UIView *rewardedIndicator;
@property (weak, nonatomic) IBOutlet UIView *nativeIndicator;

@end

static const long long bannerID = 1467528436056;
static const long long staticID = 1469962358913;
static const long long videoID = 1468075542702;
static const long long rewardedID = 1467697853357;
static const long long nativeID = 1472487971694;

@implementation ViewController

#pragma mark - Actions
#pragma mark Loading Actions

- (IBAction)loadBanner:(id)sender
{
    [self loadBannerAd];
}
- (IBAction)loadStatic:(id)sender
{
    [self loadStaticAd];
}

- (IBAction)loadVideo:(id)sender
{
    [self loadVideoAd];
}

- (IBAction)loadRewarded:(id)sender
{
    [self loadRewardedAd];
}

- (IBAction)loadNative:(id)sender
{
    [self loadNativeAd];
}

#pragma mark Show Actions

- (IBAction)showBanner:(id)sender
{
    [self showBannerAd];
}

- (IBAction)showStatic:(id)sender
{
    [self showStaticAd];
}

- (IBAction)showVideo:(id)sender
{
    [self showVideoAd];
}

- (IBAction)showRewarded:(id)sender
{
    [self showRewardedAd];
}

- (IBAction)showNative:(id)sender
{
    [self showNativeAd];
}

#pragma mark - Init

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    IMBanner *banner = [[IMBanner alloc] initWithFrame:INMOBI_BANNER_FRAME placementId:bannerID delegate:self];
    IMInterstitial *interstitialStatic = [[IMInterstitial alloc] initWithPlacementId:staticID delegate:self];
    IMInterstitial *interstitialVideo = [[IMInterstitial alloc] initWithPlacementId:videoID delegate:self];
    IMInterstitial *interstitialRewarded = [[IMInterstitial alloc] initWithPlacementId:rewardedID delegate:self];
    IMCustomNative *native = [[IMCustomNative alloc] initWithPlacementId:nativeID delegate:self];
    
    [self setBanner:banner];
    [self setInterstitialStatic:interstitialStatic];
    [self setInterstitialVideo:interstitialVideo];
    [self setInterstitialRewarded:interstitialRewarded];
    [self setNative:native];
}

#pragma mark - Loading

- (void)loadBannerAd
{
    [[self banner] removeFromSuperview];
    [[self banner] load];
}

- (void)loadStaticAd
{
    [[self interstitialStatic] load];
}

- (void)loadVideoAd
{
    [[self interstitialVideo]  load];
}

- (void)loadRewardedAd
{
    [[self interstitialRewarded] load];
}

- (void)loadNativeAd
{
    [[self native] load];
}

#pragma mark - Showing Ads

- (void)showBannerAd
{
    if ([self banner] != nil)
    {
        [[self banner] removeFromSuperview];
        [[self view] addSubview:[self banner]];
        
        [[self banner] setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:[self banner]
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:[self view]
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1.f constant:0.f];
        
        NSLayoutConstraint *horizontalCenter = [NSLayoutConstraint constraintWithItem:[self banner]
                                                                            attribute:NSLayoutAttributeCenterX
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:[self view]
                                                                            attribute:NSLayoutAttributeCenterX
                                                                           multiplier:1.f constant:0.f];
        
        [[self view] addConstraints:@[bottom, horizontalCenter]];
        
        NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:[self banner]
                                                                 attribute:NSLayoutAttributeWidth
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.f constant:CGRectGetWidth([[self banner] frame])];
        
        NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:[self banner]
                                                                  attribute:NSLayoutAttributeHeight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:nil
                                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                                 multiplier:1.f constant:CGRectGetHeight([[self banner] frame])];
        
        [[self banner] addConstraints:@[width, height]];
        [[self bannerIndicator] setBackgroundColor:[UIColor lightGrayColor]];
    }
}

- (void)showStaticAd
{
    if ([[self interstitialStatic] isReady])
    {
        [[self interstitialStatic] showFromViewController:self];
        [[self staticIndicator] setBackgroundColor:[UIColor lightGrayColor]];
    }
}

- (void)showVideoAd
{
    if ([[self interstitialVideo] isReady])
    {
        [[self interstitialVideo] showFromViewController:self];
        [[self videoIndicator] setBackgroundColor:[UIColor lightGrayColor]];
    }
}

- (void)showRewardedAd
{
    if ([[self interstitialRewarded] isReady])
    {
        [[self interstitialRewarded] showFromViewController:self];
        [[self rewardedIndicator] setBackgroundColor:[UIColor lightGrayColor]];
    }
}

- (void)showNativeAd
{
    if ([self native] != nil)
    {
        DMPKRenderView *renderView = [DMPKRenderView new];
        
        [renderView setDidShowHandler:^
         {
             [AppDelegate printClassWithName:@"IMNative"];
         }];
        
        [renderView setPrivacyActionHandler:^
         {
             // do nothing
         }];
        
        [renderView setActionHandler:^
         {
             [[self native] reportAdClickAndOpenLandingURL:nil];
         }];
        
        [self setRenderView:renderView];
        
        [[self view] addSubview:renderView];
        
        [renderView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [[self view] addConstraints:[NSLayoutConstraint
                                     constraintsWithVisualFormat:@"V:|-[renderView]-|"
                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                     metrics:nil
                                     views:NSDictionaryOfVariableBindings(renderView)]];
        
        [[self view] addConstraints:[NSLayoutConstraint
                                     constraintsWithVisualFormat:@"H:|-[renderView]-|"
                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                     metrics:nil
                                     views:NSDictionaryOfVariableBindings(renderView)]];
        
        [[self view] addSubview:renderView];
        [renderView renderAd:[self native]];
        [IMNative bindNative:[self native] toView:renderView];
        [[self nativeIndicator] setBackgroundColor:[UIColor lightGrayColor]];
    }
}

#pragma mark - IMBannerDelegate

-(void)bannerDidFinishLoading:(IMBanner*)banner
{
    [[self bannerIndicator] setBackgroundColor:[UIColor greenColor]];
}

#pragma mark - IMInterstitialDelegate

-(void)interstitialDidFinishLoading:(IMInterstitial*)interstitial
{
    long long pid = [[interstitial valueForKey:@"placementId"] longLongValue];
    
    switch (pid)
    {
        case staticID:
            [[self staticIndicator] setBackgroundColor:[UIColor greenColor]];
            break;
        case videoID:
            [[self videoIndicator] setBackgroundColor:[UIColor greenColor]];
            break;
        case rewardedID:
            [[self rewardedIndicator] setBackgroundColor:[UIColor greenColor]];
            break;
            
        default:
            break;
    }
}

-(void)interstitial:(IMInterstitial*)interstitial didInteractWithParams:(NSDictionary*)params
{
    NSLog(@"Interstitial Did Interact With Params");
}

-(void)userWillLeaveApplicationFromInterstitial:(IMInterstitial*)interstitial
{
    NSLog(@"Interstitial Will Leave Application");
}

#pragma mark - IMNativeDelegate

-(void)nativeDidFinishLoading:(IMNative*)native
{
    [[self nativeIndicator] setBackgroundColor:[UIColor greenColor]];
}

@end
